#
# Build as
#
#   docker build -t gitlab-registry.cern.ch/cholm/cc7-root-doxygen:base .
#
# This sets up a docker image
#
# - Based on CERN CentOS 7
# - With Kerboros client
# - Utility to copy to EOS
# - ROOT (including doxygen tag file)
#
# This accepts a single argument - the ROOT version
#
FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:latest
LABEL maintainer="christian.holm.christensen@cern.ch"

# Install Kerberos client, TexLive, Doxygen, libaries
RUN /usr/bin/yum install -y yum-plugin-ovl deltarpm && \
    /usr/bin/yum install -y			\
        krb5-workstation			\
	xrootd-client				\
	git					\
	gcc-c++					\
	make					\
	bzip2					\
	gcc-gfortran				\
	patch					\
	cmake					\
	openssl					\
	openssl-devel				\
	openssl-libs				\
	libAfterImage-devel			\
	m4					\
	libcurl-openssl				\
	libcurl-devel				\
	libpng-devel				\
	libjpeg-devel				\
	libSM					\
	libX11					\
	libXext					\
	libXft					\
	libXpm					\
	mesa-libGLU-devel			\
	mesa-dri-drivers			\
	glx-utils				&& \
	yum clean -y all

# ROOT version
ARG ROOT_VERSION
ENV ROOT_VERSION ${ROOT_VERSION:-6.10.02}
RUN curl -s https://root.cern.ch/download/root_v${ROOT_VERSION}.Linux-centos7-x86_64-gcc4.8.tar.gz > /opt/root.tar.gz && \
    cd /opt && tar xf root.tar.gz && rm -f root.tar.gz && \
    chown -R root:root root && \
    cd /opt/root && curl -s https://root.cern.ch/doc/master/ROOT.tag \
       > ROOT.tag 


# Get ROOT tag file
# RUN cd /usr/share/root && \
#     curl -s https://root.cern.ch/doc/master/ROOT.tag > ROOT.tag

# Set-up ROOT in all shells. 
COPY rootinit.sh /etc/profile.d/rootinit.sh
RUN chmod 755 /etc/profile.d/rootinit.sh

# Script that will rsync the output generated to an EOS folder (could
# be an EOS Web site)
COPY deploy-eos.sh /usr/local/bin/deploy-eos
RUN chmod 755 /usr/local/bin/deploy-eos

#
# EOF
#

