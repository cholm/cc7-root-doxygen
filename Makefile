#
#
#
IMAGE	:= gitlab-registry.cern.ch/cholm/cc7-root-doxygen


build:	.build-base .build-full

.build-base:	Dockerfile.base
	docker build -t $(IMAGE):base -f $< .
	touch $@ 

.build-full:	Dockerfile.full .build-base
	docker build -t $(IMAGE) -f $< .
	touch $@ 

run:	build
	docker run -ti --env=DISPLAY --volume=/tmp/.X11-unix:/tmp/.X11-unix \
		$(IMAGE) bash -l

clean:
	rm *~ .build

.PHONY:	run


#
# EOF
#
