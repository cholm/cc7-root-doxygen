Docker image for CI deploy
==========================

This image 

- is based on CERN CentOS 7
- has a Kerboros client
- has a utility to copy to EOS
- has full TeXLive installation
- Doxygen (including graphviz)
- ROOT (including doxygen tag file)

This accepts a single argument - the ROOT version.  Specify as e.g., 

  ROOT_VERSION=6.10.02

Images
------
 
- Base image. Refer to this image as 

		gitlab-registry.cern.ch/cholm/cc7-root-doxygen:base
		
  This image is based on CentOS 7, has XRootD client, Kerberos, ROOT,
  and a tool to deploy on EOS. 
  
- Full image. Refer to this image as 

		gitlab-registry.cern.ch/cholm/cc7-root-doxygen
		
  This image is based on the above image, but also has a relatively
  full TeXLive installation. 
  
Usage in other Docker images or deployment
------------------------------------------

These images are intended as base images for other images, or for
deploying software and documentation in GitLab's CI.  The base image
is mostly suitable as base image for other images, while the full
image is more for deploying software. 

### As a base image 

In your `Dockerfile` recipe, do 

	FROM gitlab-registry.cern.ch/cholm/cc7-root-doxygen:base
	
### As a deploy image 

In your `.gitlab-ci.yml` do 

	image gitlab-registry.cern.ch/cholm/cc7-root-doxygen

Standalone usage
----------------

You may also want to use these images for standalone use - i.e., to
execute ROOT in a container.  Do 

	xhost +local && \
		docker run -ti \
			--env=DISPLAY \
			--volume=/tmp/.X11-unix:/tmp/.X11-unix \
			gitlab-registry.cern.ch/cholm/cc7-root-doxygen:base \
			bash 
			
This will bring up a shell (as user _root_) from which you can execute
ROOT. 

The `xhost` command above is to ensure that the container can talk to
the host X server.  If you're running on MacOSX, you should allow
remote connections in your XQuartz settings, and modify the command
above to 

	ip=$(ipconfig getifaddr en0) \
		xhost +${ip} && \
		docker run -ti \
			--env=DISPLAY=${ip}:${DISPLAY} \
			--volume=/tmp/.X11-unix:/tmp/.X11-unix \
			gitlab-registry.cern.ch/cholm/cc7-root-doxygen:base \
			bash 
			
			
Note, OpenGL rendering may not work depending on your graphics card
and host OS (an nVidia stack is messed up, and MacOSX doesn't seem to
work either). 

Christian


